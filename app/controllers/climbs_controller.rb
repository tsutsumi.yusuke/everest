class ClimbsController < ApplicationController
  # GET /climbs

  def index
    @climbs = Climb.all

    respond_to do |format|
      format.html
      format.json { render json: @climbs }
    end
  end

  def show
    @climb = Climb.find(params[:id])
    respond_to do |format|
      format.html 
      format.json { render json: @climb }
    end
  end

  def new
    @climb = Climb.new
    @floors = '[{"from": 1, "to": 2, "stepsPerFloor": 20}, {"from": 2, "to": 43, "stepsPerFloor": 20}]'

    respond_to do |format|
      format.html 
      format.json { render json: @climb }
    end
  end

  # Disable editing for now
  #def edit
  #  @climb = Climb.find(params[:id])
  #end

  def create
    @climb = Climb.new(params[:climb])
    @climb.user_id = current_user.id
    respond_to do |format|
      if @climb.save
        logger.info "Added new climb: #{@climb.user_id} climbed #{@climb.tower_id}"
        @climb.user.total_distance += @climb.distance
        @climb.user.save!
        format.html { redirect_to @climb, notice: "Climb was succesfully created." }
        format.json { render json: @climb, status: :created, location: @climb }
      else
        format.html { render action: "new" }
        format.json { render json: @climb.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @climb = Climb.find(params[:id])

    respond_to do |format|
      if @climb.update_attributes(params[:tower])
        format.html { redirect_to @climb, notice: 'Tower was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @climb.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /towers/1
  # DELETE /towers/1.json
  def destroy
    @climb = Climb.find(params[:id])
    @climb.user.total_distance -= @climb.distance
    @climb.user.save!
    @climb.destroy

    respond_to do |format|
      format.html { redirect_to climbs_url }
      format.json { head :no_content } end
  end
end
