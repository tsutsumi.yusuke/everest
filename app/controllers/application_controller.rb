class ApplicationController < ActionController::Base
  protect_from_forgery

  private

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def pretty_time(date)
    date.strftime("%B %e, %Y at %I:%M %P")
  end

  # gives a comparison of the distance travelled
  def distance_comparison(distance)
    distances = {
      0 => { :name => "dummy" },
      829.8 => { :name => "Burj Khalifa", :link => "http://en.wikipedia.org/wiki/Burj_Khalifa" },
      1745  => { :name => "Mt.Rushmore", :link => "http://en.wikipedia.org/wiki/Mount_Rushmore" },
      4392  => { :name => "Mt.Rainier", :link => "http://en.wikipedia.org/wiki/Mount_Rainier" },
      5895  => { :name => "Mt.Kilimanjaro", :link => "http://en.wikipedia.org/wiki/Mount_Kilimanjaro" },
      8848  => { :name => "Mt.Everest", :link => "http://en.wikipedia.org/wiki/Mount_Everest" }
    }

    keys = distances.keys.sort
    for i in 1..(keys.length - 1)
      logger.info(keys[i - 1])
      if (keys[i - 1]..keys[i]).member?(distance)
        return "that's #{ (100 * distance / keys[i]).round(2).to_s }% of the " + 
          "height of the <a href='#{ distances[keys[i]][:link] }'>" +
            distances[keys[i]][:name] + "</a>!"
      end
    end
    
=begin
    case distance
      when 0...829.8
        "that's " + (100 * distance / 829.8).round(2).to_s + "% of the height of the Burj Khalifa!"
      when 829.8...1745
        "that's " + (100 * distance / 1745).round(2).to_s + "% of the height of Mt.Rushmore!"
      when 1745...4392
        "that's " + (100 * distance / 4392).round(2).to_s + "% of the height of Mt.Rainier!"
      when 4392...5895
        "that's " + (100 * distance / 5895).round(2).to_s + "% of the height of Mt.Kilimanjaro!"
      when 5895...8848
        "that's " + (100 * distance / 8848).round(2).to_s + "% of the height of Mt.Everest!"
      else
        "nope"
      end
=end
  end

  helper_method :current_user, :pretty_time, :distance_comparison
end
