class SessionsController < ApplicationController
  def new
  end
  
  def create_omniauth
    #user = User.find_by_email(params[:email])
    #if user && user.authenticate(params[:password])
    #  session[:user_id] = user.id
    #  redirect_to "/"
    #else
    #  flash.now.alert = "Invalid email or password"
    #  render "new"
    #end
    user = User.from_omniauth(env["omniauth.auth"])
    session[:user_id] = user.id
    redirect_to root_url
  end

  def create
    user = User.find_by_email(params[:email])
    if user && user.authenticate(params[:password])
      session[:user_id] = user.id
      redirect_to root_url
    else
      redirect_to login_url, :error => "Invalid email or password"
    end
  end

  def destroy 
    session[:user_id] = nil
    redirect_to root_url
  end
end
