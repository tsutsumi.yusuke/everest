class UsersController < ApplicationController
  def new
    @user = User.new
  end

  # Ideally, it'll be a dynamic profile/edit page generator
  def profile
    @user = current_user
    if current_user
      render "profile"
    else
      redirect_to root_url
    end
  end

  def login
  end


  def update 
    @user = current_user
    if current_user
      respond_to do |format|
        if @user.update_attributes(params[:user])
          format.html { redirect_to profile_path, notice: 'User was successfully updated.' }
          format.json { head :no_content }
        else
          format.html { render action: "profile" }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to root_url
    end
  end


  # need to validate that totalsteps isn't passed as a param
  def create
    @user = User.new(params[:user])
    if @user.save
      session[:user_id] = @user.id
      redirect_to root_url
    else
      render "new"
    end
  end

  # GET /towers/1
  # GET /towers/1.json
  def show
    @user = User.find(params[:id])
    @climbs = Climb.where(:user_id => @user.id)

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @user }
    end
  end

end
