module ApplicationHelper

  def signin_path(provider)
    root_url + "auth/#{provider.to_s}"
  end
end
