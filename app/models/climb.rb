class Climb < ActiveRecord::Base
  belongs_to :tower
  belongs_to :user
  attr_accessible :tower, :tower_id, :user, :user_id, :distance, :created_at, :time

  validates :distance, :presence => true
  validates_numericality_of :distance, :greater_than => 0

  def steps
    (self.distance / self.tower.step_height).round
  end
end
