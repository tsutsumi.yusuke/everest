class Tower < ActiveRecord::Base
  extend FriendlyId
  attr_accessible :description, :name, :tag, :total_steps, :photo, :step_height
  friendly_id :tag
  has_attached_file :photo, :styles => { :large => "360x640", :small => "180x320" }
  
  validates :name, :presence => true
  validates :total_steps, :presence => true 
  validates :step_height, :presence => true 

  def pretty_steps
    # pretty print steps, with step included with value.
    return self.total_steps.to_s + " " +
      (self.total_steps == 1 ? "step" : "steps")
  end

  def pretty_step_height
    # pretty print step height
    return self.step_height.to_s + "m"
  end

  def height
    return self.total_steps * self.step_height
  end

  def pretty_height
    self.height.to_s + "m"
  end

end
