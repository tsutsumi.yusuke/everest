class User < ActiveRecord::Base
  extend FriendlyId
  friendly_id :username
  attr_accessible :email, :name, :username, :total_distance, :uid, :image
  validates_presence_of :name
  validate :name_must_be_continuous_chars
  validate :password_if_no_omniauth, :on => :create
  validates :username, :uniqueness => true, :presence => true
  validates :email, :uniqueness => true
  has_attached_file :image, :styles => { :large => "400x400", :medium => "200x200", :small => "100x100" }
  attr_accessible :email, :name, :password, :password_confirmation, :total_distance
  has_secure_password(:validations => false)

  # Validates the name is only alphanumeric/underscores/dashes:
  def name_must_be_continuous_chars
    unless %r{^[a-zA-Z0-9_\-]*$}xi.match(username)
      errors.add(:name, "Name must only contain letters, numbers, dashes, and underscores")
    end
  end

  def password_if_no_omniauth
    unless uid != nil
      errors.add(:password, "A password is required!")
    end
  end

  def self.from_omniauth(auth)
    where(auth.slice(:provider, :uid)).first_or_initialize.tap do |user|
      user.provider = auth.provider
      user.uid = auth.uid
      user.name = auth.info.name
      if auth.info.image 
        logger.info "User image url is #{auth.info.image}"
        user.image = self.load_image(auth.info.image, auth.provider)
      end
      user.username = "yusuke"
      user.oauth_token = auth.credentials.token
      user.oauth_expires_at = Time.at(auth.credentials.expires_at)
      user.save!
    end
  end

  def self.load_image(url, provider)
      if provider == "facebook"
        url = url.gsub /\??type=square$/, '?width=400&height=400'
        logger.info url
        open(url)
      end
  end
end

