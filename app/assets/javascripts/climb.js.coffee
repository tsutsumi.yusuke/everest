# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

class Stairs
  constructor: (@div, @floors) ->

  initialize: =>
    (@addStairs(s) for s in @floors)

  addStairs: (@floor) =>
    ($(@div).append("<div class='floor'></div>") for i in [@floor.from...@floor.to])


#stairs = new Stairs("#stairs", test)
#stairs.initialize()
