# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130313081625) do

  create_table "achievements", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "climbs", :force => true do |t|
    t.integer  "tower_id"
    t.integer  "user_id"
    t.decimal  "distance",   :precision => 10, :scale => 2, :default => 0.0, :null => false
    t.datetime "created_at",                                                 :null => false
    t.datetime "updated_at",                                                 :null => false
    t.decimal  "time",                                      :default => 0.0
  end

  add_index "climbs", ["tower_id"], :name => "index_climbs_on_tower_id"
  add_index "climbs", ["user_id"], :name => "index_climbs_on_user_id"

  create_table "towers", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "total_steps"
    t.datetime "created_at",                                                         :null => false
    t.datetime "updated_at",                                                         :null => false
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.string   "tag"
    t.decimal  "step_height",        :precision => 10, :scale => 2, :default => 0.0, :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "email"
    t.datetime "created_at",                                                           :null => false
    t.datetime "updated_at",                                                           :null => false
    t.string   "name"
    t.decimal  "total_distance",     :precision => 10, :scale => 2, :default => 0.0,   :null => false
    t.string   "uid"
    t.string   "provider"
    t.string   "oauth_token"
    t.datetime "oauth_expires_at"
    t.string   "username"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.boolean  "is_admin",                                          :default => false
    t.string   "password_digest"
  end

end
