class CreateClimbs < ActiveRecord::Migration
  def change
    create_table :climbs do |t|
      t.references :tower
      t.references :user
      t.decimal :distance, :default => 0, :null => false, :scale => 2, :precision => 10

      t.timestamps
    end
    add_index :climbs, :tower_id
    add_index :climbs, :user_id
  end
end
