class ChangeTotaldistanceName < ActiveRecord::Migration
  def up
    rename_column :users, :totaldistance, :total_distance
  end

  def down
    rename_column :users, :total_distance, :totaldistance
  end
end
