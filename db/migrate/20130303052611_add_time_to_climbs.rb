class AddTimeToClimbs < ActiveRecord::Migration
  def change
    add_column :climbs, :time, :decimal, :default => 0
  end
end
