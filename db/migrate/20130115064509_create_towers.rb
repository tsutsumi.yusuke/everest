class CreateTowers < ActiveRecord::Migration
  def change
    create_table :towers do |t|
      t.string :name
      t.string :description
      t.integer :total_steps

      t.timestamps
    end
  end
end
