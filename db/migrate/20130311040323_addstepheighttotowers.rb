class Addstepheighttotowers < ActiveRecord::Migration
  def up
    add_column :towers, :step_height, :decimal, :precision => 10, :scale => 2, :default =>  0.0, :null => false
  end

  def down
    remove_column :towers, :step_height
  end
end
