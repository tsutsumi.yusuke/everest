class AddadmintoUsers < ActiveRecord::Migration
  def up
    add_column :Users, :is_admin, :boolean, :default => 0
  end

  def down
    remove_column :Users, :is_admin
  end
end
