class AddAttachmentPhotoToTowers < ActiveRecord::Migration
  def self.up
    change_table :towers do |t|
      t.attachment :photo
    end
  end

  def self.down
    drop_attached_file :towers, :photo
  end
end
