class Changefacebookcredsuser < ActiveRecord::Migration
  def up
    rename_column :users, :fbid, :uid
  end

  def down
    rename_column :users, :uid, :fbid
  end
end
