class ChangeTotaldistanceDefault < ActiveRecord::Migration
  def up
    change_column :users, :total_distance, :decimal, :default => 0, :null => false, :scale => 2, :precision => 10
  end

  def down
    change_column :users, :total_distance, :float
  end
end
