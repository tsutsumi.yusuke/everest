class AddTotaldistanceToUser < ActiveRecord::Migration
  def change
    add_column :users, :totaldistance, :float
  end
end
