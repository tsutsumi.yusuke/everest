everest
=======

Stair Climbing app.

install
=======
Node needs to be installed:

    $ sudo apt-get install node

Imagemagick is also necessary:

    $ sudo apt-get install imagemagick

For some reason bcrypt ruby had to be installed manually beforehand:

    $ gem install bcrypt-ruby

install gems:

    $ bundle install 

setup db:

    $ rake db:migrate

run server:

    $ rails server



TODO
======
* Achievements
* Leaderbords
* Allow multiple buildings
* "Kills"
* look at canvasjs for charts: http://canvasjs.com/
* also this for charts: https://github.com/filamentgroup/jQuery-Visualize

