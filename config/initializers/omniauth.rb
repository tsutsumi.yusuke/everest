OmniAuth.config.logger = Rails.logger

# Following this guide for omniauth, but should work on the rest of the featureset first.
# http://railscasts.com/episodes/360-facebook-authentication?view=asciicast
Rails.application.config.middleware.use OmniAuth::Builder do
  provider :facebook, Rails.application.config.facebook_appid, Rails.application.config.facebook_appsecret
end
